import '../../scss/output_modules/episodes-cards.scss';

window.addEventListener('load', () => {
  if (document.querySelector('.category-seasons')) {
    const activeElement = document.querySelector('.translation-slider .category-seasons .item [aria-selected="true"]');
    const initialSlide = [...activeElement.parentElement.parentElement.children].indexOf(activeElement.parentElement);

    new window.Swiper('.category-seasons .swiper-container', {
      centeredSlides: true,
      spaceBetween: 8,
      initialSlide,
      slidesPerView: 'auto',
      navigation: {
        nextEl: '.category-seasons .swiper-button-next',
        prevEl: '.category-seasons .swiper-button-prev',
      },
    });
  }
});