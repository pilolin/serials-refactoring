import Swiper from 'swiper';
import '../../scss/output_modules/recommend-serials.scss';

window.addEventListener('load', () => {
  if (document.querySelector('.recommend-serials-slider')) {
    new Swiper('.recommend-serials-slider', {
      spaceBetween: 12,
      slidesPerView: 2.5,
      breakpoints: {
        768: {
          slidesPerView: 3.5,
          spaceBetween: 26,
        },
        992: {
          slidesPerView: 5,
          spaceBetween: 26,
        },
      }
    });
  }
})