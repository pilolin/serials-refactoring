import MicroModal from 'micromodal';
import lozad from 'lozad';
import Swiper, { Navigation } from 'swiper';
import '../../scss/output_modules/styles.scss';

Swiper.use([Navigation]);

window.Swiper = Swiper;

window.getCookie = function(name) {
    const matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([.$?*|{}()\[\]\\\/+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

window.setCookie = function(name, value, options = {}) {

    options = {
        path: '/',
        ...options
    };

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (const optionKey in options) {
        updatedCookie += "; " + optionKey;
        const optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

window.deleteCookie = function(name) {
    setCookie(name, "", { 'max-age': -1 });
}

window.addEventListener('load', () => {
    if (document.querySelector('.sounds-tabs')) {
        new Swiper('.sounds-tabs .swiper-container', {
            spaceBetween: 8,
            slidesPerView: 'auto',
            navigation: {
                nextEl: '.sounds-tabs .swiper-button-next',
                prevEl: '.sounds-tabs .swiper-button-prev',
            },
        });
    }
})

lozad('.lazy', {
    loaded: function(el) {
        el.classList.add('loaded');
    },
}).observe();

MicroModal.init({
    openTrigger: 'data-micromodal-open',
    closeTrigger: 'data-micromodal-close',
    openClass: 'is-open',
    disableScroll: true,
    awaitCloseAnimation: true,
    onClose: function(modal) {
        setTimeout(function() {
            const iframe = modal.querySelector('iframe');
            iframe.src = iframe.src;
        }, 320);
    },
});

document.addEventListener('click', function(e) {
    if (!e.target.closest('.btn-menu-mobile')) return;

    e.preventDefault();

    document.body.classList.toggle('menu-open');
});

document.addEventListener('click', function(e) {
    if (!e.target.closest('.box-close-menu')) return;

    e.preventDefault();

    document.body.classList.remove('menu-open');
});

document.addEventListener('click', function(e) {
    if (!e.target.closest('.btn-menu')) return;

    e.preventDefault();

    e.target.closest('.btn-menu').classList.toggle('active');
    document.querySelector('.drop-header').classList.toggle('active');
    document.querySelector('.close-menu-field').classList.toggle('active');
    document.querySelector('.social-block').classList.remove('open');
});

document.addEventListener('click', function(e) {
    if (!e.target.closest('.close-menu-field')) return;

    e.preventDefault();

    e.target.closest('.close-menu-field').classList.remove('active', 'open');
    document.querySelector('.drop-header').classList.remove('active');
    document.querySelector('.btn-menu').classList.remove('active');
    document.querySelector('.social-block').classList.remove('open');
});


const scrollToBtn = document.querySelector('.scrollToBtn');

if (scrollToBtn) {
    document.addEventListener('click', function(e) {
        const targetLink = e.target.closest('.scrollToBtn');

        if (!targetLink) return;

        e.preventDefault();

        window.scrollTo({
            top: pageYOffset + document.querySelector(targetLink.hash).getBoundingClientRect().top,
            left: 0,
            behavior: 'smooth'
        });
    });
}