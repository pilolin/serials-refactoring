import '../../scss/output_modules/episode-page.scss';


function getCoords(elem) { // кроме IE8-
  var box = elem.getBoundingClientRect();

  return {
    top: box.top + pageYOffset,
    left: box.left + pageXOffset
  };
}

window.addEventListener('load', () => {
  new Swiper('.sounds-tabs', {
    slidesPerView: 'auto',
  });
});

const tab = window.location.hash.replace('#', '');

if (tab && document.querySelector('.' + tab + ' .one-item')) {
  const id_video = document.querySelector('.' + tab + ' .one-item').dataset.sound;

  document.querySelector('.one-item[aria-selected="true"]').setAttribute('aria-selected', 'false');

  document.querySelector('.' + tab + ' .one-item').setAttribute('aria-selected', 'true');
  document.getElementById('iframe-player').src = 'https:' + id_video;
}

document.addEventListener('click', function(e) {
  if (!e.target.closest('.one-serial-dark a.one-item')) return;

  const elem = e.target.closest('.one-serial-dark a.one-item');
  const id_video = elem.dataset.sound;
  const widthItem = elem.parentElement.clientWidth;
  const widthBody = document.body.clientWidth;
  const offsetLeft = getCoords(elem).left;

  if (widthBody < offsetLeft + widthItem + 35) {
    elem.closest('.episode-sounds').swiper.slideNext();
  }

  document.querySelector('.one-item[aria-selected="true"]').setAttribute('aria-selected', 'false');

  elem.setAttribute('aria-selected', 'true');

  document.getElementById('iframe-player').src = 'https:' + id_video;

  e.preventDefault();
});

document.addEventListener('click', function(e) {
  if (!e.target.closest('.one-serial-information .under-article span a')) return;

  const link = e.target.closest('.one-serial-information .under-article span a');
  const soundsId = link.dataset.soundId;

  document.querySelector('.one-serial-dark a.one-item[data-sound-id="'+soundsId+'"]').click();

  window.scrollTo({
    top: 0,
    left: 0,
    behavior: 'smooth',
  });
  e.preventDefault();
});

