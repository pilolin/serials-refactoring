import '../../scss/output_modules/sim-movie.scss';


window.addEventListener('load', () => {
  if (document.querySelector('.season-list')) {
    new Swiper('.season-list .swiper-container', {
      navigation: {
        nextEl: '.season-list .swiper-button-next',
        prevEl: '.season-list .swiper-button-prev',
      },
      slidesPerView: 5,
      breakpoints: {
        600: { slidesPerView: 8 },
      },
    });
  }
})