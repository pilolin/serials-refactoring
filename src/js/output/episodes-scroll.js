import 'simplebar';
import '../../scss/output_modules/episodes-scroll.scss';

if (document.querySelector('.main-list-series')) {
  const stepScroll = document.querySelector('.main-list-series .one-item-series').clientWidth / 2;

  document.addEventListener('click', function(e) {
    if (!e.target.closest('.big-slider-main .series-btn[data-role]')) return;

    const currentScroll = document.querySelector('.big-slider-main[aria-selected="true"] .simplebar-content-wrapper').scrollLeft

    if (e.target.closest('.big-slider-main .series-btn[data-role]').dataset.role === 'scrollNext') {
      document.querySelector('.big-slider-main[aria-selected="true"] .simplebar-content-wrapper').scrollTo(currentScroll + stepScroll, 0)
    } else {
      document.querySelector('.big-slider-main[aria-selected="true"] .simplebar-content-wrapper').scrollTo(currentScroll - stepScroll, 0)
    }

    e.preventDefault();
  });
}

window.addEventListener('load', () => {
  new Swiper('.all-seasons', {
    slidesPerView: 'auto',
  });
});

document.addEventListener('click', function(e) {
  if (!e.target.closest('.slider-main-box a.one-item')) return;

  e.preventDefault();

  if (!e.target.closest('a[aria-selected="false"]')) return;

  const linkElem = e.target.closest('a.one-item');
  const targetSeasonId = linkElem.hash;
  const linkFirstEpisodeElem = document.querySelector('.first-series-btn');
  const targetSeasonNumber = linkElem.dataset.seasonNumber;

  const activeLinkElem = document.querySelector('.slider-main-box a.one-item[aria-selected="true"]');
  const activeSeasonElem = document.querySelector('.big-slider-main[aria-selected="true"]');

  if(document.querySelector(targetSeasonId)) {
    activeSeasonElem.setAttribute('aria-selected', 'false');
    activeLinkElem.setAttribute('aria-selected', 'false');

    document.querySelector(targetSeasonId).setAttribute('aria-selected', 'true');
    linkElem.setAttribute('aria-selected', 'true');
    linkFirstEpisodeElem.href = document.querySelector(targetSeasonId+' .one-item-series:last-of-type').href;
    document.querySelector(targetSeasonId+' .simplebar-content-wrapper').scrollTo(0, 0);

    const sliderSeasons = linkElem.closest('.swiper-container');
    const indexActiveElem = Array.prototype.slice.call(sliderSeasons.querySelectorAll('.swiper-slide')).indexOf(linkElem.parentElement);

    sliderSeasons.swiper.slideTo(indexActiveElem);
  } else {
    fetch('/engine/ajax/home_slide.php?season_number='+targetSeasonNumber)
      .then((response) => response.json())
      .then((res) => {
        if (res.success && res.list) {
          activeLinkElem.setAttribute('aria-selected', 'false');
          linkElem.setAttribute('aria-selected', 'true');

          const sliderSeasons = linkElem.closest('.swiper-container');
          const indexActiveElem = Array.prototype.slice.call(sliderSeasons.querySelectorAll('.swiper-slide')).indexOf(linkElem.parentElement);

          sliderSeasons.swiper.slideTo(indexActiveElem);

          activeSeasonElem.setAttribute('aria-selected', 'false');
          activeSeasonElem.insertAdjacentHTML('afterend', res.list);

          linkFirstEpisodeElem.href = document.querySelector(targetSeasonId+' .one-item-series:last-of-type').href;
        }
      });
  }
});