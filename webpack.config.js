const path = require('path');
const argv = require('yargs').argv;
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const pages = require('./pages');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const CopyPlugin = require('copy-webpack-plugin');
require('html-webpack-plugin');
require('webpack');
require('@babel/register');
require('@babel/preset-env');

const isDevelopment = argv.mode === 'development';
const isProduction = !isDevelopment;
const distPath = path.join(__dirname, '/dist');

const extractTextPluginOptions = ExtractTextPlugin.extract({
  fallback: 'style-loader',
  use: [
    { loader: 'css-loader', options: { sourceMap: true } },
    {
      loader: 'postcss-loader',
      options: {
        plugins: [autoprefixer({ browsers:['ie >= 11', 'last 4 version'] })],
        sourceMap: true
      }
    },
    {
      loader: 'sass-loader',
      options: { sourceMap: true, includePaths: ["/images/"] }
    }
  ]
});

const config = {
  mode: 'development',
  entry: {
    main: ['@babel/polyfill','./src/js/output/index.js'],
    episodesCards: ['@babel/polyfill','./src/js/output/episodes-cards.js'],
    episodesRows: ['@babel/polyfill','./src/js/output/episodes-rows.js'],
    episodesScroll: ['@babel/polyfill','./src/js/output/episodes-scroll.js'],
    simMovie: ['@babel/polyfill','./src/js/output/sim-movie.js'],
    episodePage: ['@babel/polyfill','./src/js/output/episode-page.js'],
    topSerials: ['@babel/polyfill','./src/js/output/recommend-serials.js'],
  },
  output: {
    filename: '[name].js',
    path: distPath
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }]
    }, {
      test: /\.scss$/,
      exclude: /node_modules/,
      use: extractTextPluginOptions
    }, {
      test: /\.(gif|png|jpe?g|svg)$/i,
      include: [
        path.resolve(__dirname, "src/images")
      ],
      use: [
        {
          loader: 'file-loader',
          options: { name: 'images/[name][hash].[ext]' }
        }, {
          loader: 'image-webpack-loader',
          options: {
            mozjpeg: { progressive: true, quality: 70 }
          }
        },
        ],
      }, {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        include: [
          path.resolve(__dirname, "src/fonts")
        ],
        use: {
          loader: 'file-loader',
          options: { name: 'fonts/[name][hash].[ext]' }
        },
      }, {
        test: /\.(pug|jade)$/,
        loader: 'pug-loader',
        options: { pretty: true }
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '[name].css',
      disable: isDevelopment
    }),
    ...pages,
    new CopyPlugin([
      { from: './src/images', to: './images' },
      { from: './src/fonts', to: './fonts' },
    ]),
  ],
  optimization: isProduction ? {
    minimizer: [
      new UglifyJsPlugin({
        sourceMap: true,
        uglifyOptions: {
          compress: {
            inline: false,
            warnings: false,
            drop_console: true,
            unsafe: true
          },
        },
      }),
      new OptimizeCSSAssetsPlugin(),
    ],
  } : {},
  devServer: {
    contentBase: distPath,
    port: 9000,
    compress: true,
    open: true
  }
};

module.exports = config;