const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
	new HtmlWebpackPlugin({
		template: './src/index.pug',
		filename: './index.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/category-1.pug',
		filename: './category-1.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/category-2.pug',
		filename: './category-2.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/episode.pug',
		filename: './episode.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/index-comics.pug',
		filename: './index-comics.html'
	}),
]